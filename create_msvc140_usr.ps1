$usr_dir = "usr"
$usr_exists = Test-Path usr

if (-Not $usr_exists) {
	Write-Host "Creating directory `"$usr_dir`"."
        New-Item -ItemType directory -Path usr
}

Write-Host "Copying files to `"$usr_dir`"..."
Copy-Item -Force -Recurse -Path msvc\vc140\* -Destination usr

Write-Host "Creation of `"$usr_dir`" done. Press anything to close."
$host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
